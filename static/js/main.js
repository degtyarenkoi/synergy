function showError(container, errorMessage) {
      container.className = 'error';
      var msgElem = document.createElement('span');
      msgElem.className = "error-message";
      msgElem.innerHTML = errorMessage;
      container.appendChild(msgElem);
}

function resetError(container) {
      container.className = '';
      if (container.lastChild.className == "error-message") {
        container.removeChild(container.lastChild);
      }
}


function validate(e, form) {
      var elems = form.elements;

      mobilephone_pattern = /\+[0-9]+/;
        
      resetError(elems.usrname.parentNode);
      resetError(elems.email.parentNode);
      resetError(elems.phone.parentNode);
      resetError(elems.mobile.parentNode);

      if (!elems.usrname.value) {
        showError(elems.usrname.parentNode, ' This field is required');
	e.preventDefault();
      }
      else {
	usrname = elems.usrname.value;
	usrname_pattern = /[a-z]+/i;
	res = usrname_pattern.test(usrname);
	if(res == false){
	  showError(elems.usrname.parentNode, ' Name must contain just letters');
	  e.preventDefault();
	}
      }

      
      if (!elems.email.value) {
        showError(elems.email.parentNode, ' Please enter a valid email address');
	e.preventDefault();
      }
      else {
	email = elems.email.value;
	email_pattern = /[0-9a-z_]+@[0-9a-z_]+\.[a-z]{2,5}/i;
	res = email_pattern.test(email);
	if(res == false){
	  showError(elems.email.parentNode, ' An email have to be such like igor123@gmail.com');
	  e.preventDefault();
	}
      }
      



      if (elems.phone.value) {
 	phone = elems.phone.value;
        res_phone = mobilephone_pattern.test(phone);
        if(res_phone == false){
          showError(elems.phone.parentNode, 'Phone number must be in international format like +380501234567');
          e.preventDefault();
        }
      }


      if (elems.mobile.value) {
	mobile = elems.mobile.value;
	res_mobile = mobilephone_pattern.test(mobile);
	if(res_mobile == false){
          showError(elems.mobile.parentNode, ' Mobile number must be in international format like +380501234567');
          e.preventDefault();
      	}
      }  
}


