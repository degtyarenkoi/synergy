/* procedure creating table users */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS create_users_table;

CREATE PROCEDURE create_users_table()
BEGIN
    CREATE TABLE users (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30) NOT NULL,
	email VARCHAR(30) NOT NULL,
	phone VARCHAR(50),
	mobile VARCHAR(50),
	status INT(2)
    ); 
END$$

DELIMITER ;
/* END procedure creating table users */


/* procedure creating table courses */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS create_courses_table;

CREATE PROCEDURE create_courses_table()
BEGIN
    CREATE TABLE courses (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30) NOT NULL,
	code VARCHAR(30) NOT NULL
    ); 
END$$

DELIMITER ;
/* END procedure creating table courses */



/* procedure creating table courses_of_user */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS usercourses;

CREATE PROCEDURE usercourses()
BEGIN
    CREATE TABLE usercourses (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30) NOT NULL,
	user_id INT
    ); 
END$$

DELIMITER ;
/* END procedure creating table courses_of_user */


/* procedure inserting defaul values into courses table */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS new_record_courses;

CREATE PROCEDURE new_record_courses()
BEGIN
    INSERT INTO courses (id, name, code) VALUES(NULL, 'Python basics', 'p4df23');
    INSERT INTO courses (id, name, code) VALUES(NULL, 'Tornado', '234r5t');
    INSERT INTO courses (id, name, code) VALUES(NULL, 'AsyncIO', 'poi23o');
    INSERT INTO courses (id, name, code) VALUES(NULL, 'Django framework', '23423w');
    INSERT INTO courses (id, name, code) VALUES(NULL, 'Celery', '89er98');
END$$

DELIMITER ;
/* END procedure inserting defaul values into courses table */



/* procedure find all users */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS find_all;

CREATE PROCEDURE find_all()
BEGIN
    SELECT * FROM users;
END$$

DELIMITER ;
/* END procedure find all users */



/* search procedure by name */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS find_query;

CREATE PROCEDURE find_query(IN q VARCHAR(10))
BEGIN
    SELECT * FROM users WHERE name=q;
END$$

DELIMITER ;
/* END search procedure by name */



/* filter procedure by count of records */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS find;

CREATE PROCEDURE find(IN c INT)
BEGIN
    SELECT * FROM users LIMIT c;
END$$

DELIMITER ;
/* END search procedure by count of records */



/* procedure deleting record */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS delete_record;

CREATE PROCEDURE delete_record(IN user_id INT)
BEGIN
    DELETE FROM users WHERE id=user_id;
END$$

DELIMITER ;
/* END procedure deleting record */



/* procedure creating record */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS new_record;

CREATE PROCEDURE new_record(IN name VARCHAR(30), IN email VARCHAR(30), IN phone VARCHAR(30), IN mobile VARCHAR(30), IN status INT)
BEGIN
    INSERT INTO users VALUES(NULL, name, email, phone, mobile, status);
END$$

DELIMITER ;
/* END procedure creating record */



/* procedure finding record by id */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS find_record;

CREATE PROCEDURE find_record(IN user_id INT)
BEGIN
    SELECT * FROM users WHERE id=user_id;
END$$

DELIMITER ;
/* END procedure finding record by id */



/* procedure updating record by id */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS update_record;

CREATE PROCEDURE update_record(IN user_id INT, IN name VARCHAR(30), IN email VARCHAR(30), IN phone VARCHAR(30), IN mobile VARCHAR(30), IN status INT)
BEGIN
    UPDATE users  SET name=name, email=email, phone=phone, mobile=mobile, status=status WHERE id=user_id; 
END$$

DELIMITER ;
/* END procedure updating record by id */



/* procedure creating course */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS new_record_course;

CREATE PROCEDURE new_record_course(IN name VARCHAR(30), IN user_id INT)
BEGIN
    INSERT INTO usercourses VALUES(NULL, name, user_id);
END$$

DELIMITER ;
/* END procedure creating course */



/* procedure find all usercourses */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS find_usercourses;

CREATE PROCEDURE find_usercourses()
BEGIN
    SELECT * FROM usercourses;
END$$

DELIMITER ;
/* END procedure find all usercourses */



/* procedure find all courses */
DELIMITER $$

USE synergy$$

DROP PROCEDURE IF EXISTS find_all_courses;

CREATE PROCEDURE find_all_courses()
BEGIN
    SELECT * FROM courses;
END$$

DELIMITER ;
/* END procedure find all courses */
