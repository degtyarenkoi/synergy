* $ mkdir test
* $ cd test
* $ git clone git@bitbucket.org:degtyarenkoi/synergy.git
* $ virtualenv venv
* $ source venv/bin/activate
* $ cd synergy
* $ pip install -r req.txt
* $ sudo apt-get install python-mysql.connector
* $ sudo apt-get install redis-server
* $ mysql -u username -p 
* mysql> create database synergy;
* mysql> use synergy;
* $ mysql -u username -p < deploy.sql
* mysql> call create_users_table();
* mysql> call create_courses_table();
* mysql> call usercourses();
* mysql> call create_users_table();
* $ python manage.py runserver
* follow the http://localhost:8000/*