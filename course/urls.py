from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.main),
    url(r'^courses/$', views.courses),
    url(r'^users/$', views.users),
    url(r'^users/add/$', views.add),
    url(r'^page/(\d+)/$', views.users),
    url(r'^users/delete/(?P<user_id>\d+)/$', views.delete),
    url(r'^users/update/(?P<user_id>\d+)/$', views.update),
    url(r'^receiver/$', views.receiver),
]

