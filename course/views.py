from django.shortcuts import render_to_response, redirect, HttpResponse
from django.template import RequestContext

from mysql.connector import MySQLConnection

from django.core.paginator import Paginator

from django.core.cache import cache

# Create your views here.

DB = 'synergy'
USER = 'root'
PASSWORD = 'root'
HOST = 'localhost'


def main(request):
    return redirect('/users/')


def users(request, page_number=1):
    conn = MySQLConnection(db=DB, user=USER, passwd=PASSWORD, host=HOST)
    c = conn.cursor()
    if request.method == 'GET':
        q = request.GET.get('q')
        if q:
            c.callproc('find_query', args=[q])
            result = c.stored_results()
            l = get_list(result)
        else:
            c.callproc('find_all')
            result = c.stored_results()
            l = get_list(result)  
    elif request.method == 'POST':
        q = request.POST.get('usersselect')
        if q:
            c.callproc('find', args=[q])
            result = c.stored_results()
            l = get_list(result) 
            current_page = Paginator(l, q)
            return render_to_response('users.html', {'list':current_page.page(page_number)}, RequestContext(request))
    current_page = Paginator(l, 10)
    return render_to_response('users.html', {'list':current_page.page(page_number)}, RequestContext(request))


def get_list(result):
    res = []
    for k in result:
        for key in k.fetchall():
            res.append(key)
    l = []
    d = {}   
    for r in res:
        d['id'] = r[0]
        d['name'] = r[1]
        d['email'] = r[2]
        d['phone'] = r[3]
        d['mobile'] = r[4]
        d['status'] = r[5]
        l.append(d.copy())
    return l

def get_list_courses(result):
    res = []
    for k in result:
        for key in k.fetchall():
            res.append(key)
    l = []
    d = {}   
    for r in res:
        d['id'] = r[0]
        d['name'] = r[1]
        d['code'] = r[2]
        l.append(d.copy())
    return l


def courses(request):
    if(cache.get('courses_list') != None):
        l_redis = cache.get('courses_list')
        return render_to_response('courses.html', {'list':l_redis}, RequestContext(request))
    else:
        conn = MySQLConnection(db=DB, user=USER, passwd=PASSWORD, host=HOST)
        cursor = conn.cursor()
        cursor.execute('show columns from courses;')
        columns = cursor.fetchall()

        cursor.callproc('find_all_courses')
        result = cursor.stored_results()
        l = get_list_courses(result)

        cache.set('courses_list', l)
        return render_to_response('courses.html', {'list':cache.get('courses_list')}, RequestContext(request))


def delete(request, user_id):
    conn = MySQLConnection(db=DB, user=USER, passwd=PASSWORD, host=HOST)
    c = conn.cursor()
    c.callproc('delete_record', args=[user_id])
    conn.commit()
    return redirect('/')

def add(request):
    if request.method == 'POST':
        name = request.POST.get('usrname')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        mobile = request.POST.get('mobile')
        status = request.POST.get('status')
        conn = MySQLConnection(db=DB, user=USER, passwd=PASSWORD, host=HOST)
        c = conn.cursor()
        c.callproc('new_record', args=[name, email, phone, mobile, status])
        conn.commit();
        return redirect('/')
    return render_to_response('create.html', {}, RequestContext(request))

def update(request, user_id):
    conn = MySQLConnection(db=DB, user=USER, passwd=PASSWORD, host=HOST)
    c = conn.cursor()
    c.callproc('find_record', args=[user_id])
    result = c.stored_results()
    l = get_list(result)
    
    c.execute('show columns from users;')
    columns = c.fetchall()
    c.callproc('find_all_courses')
    result = c.stored_results()
    course = get_list_courses(result)
    if request.method == 'POST':
        name = request.POST.get('usrname')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        mobile = request.POST.get('mobile')
        status = request.POST.get('status')
        c.callproc('update_record', args=[user_id, name, email, phone, mobile, status])
        conn.commit();
        return redirect('/')
    return render_to_response('update.html', {'list':l, 'course':course}, RequestContext(request))


def receiver(request):
    name = request.GET.get('name')
    user_id = request.GET.get('user_id')
    
    conn = MySQLConnection(db=DB, user=USER, passwd=PASSWORD, host=HOST)
    c = conn.cursor()

    c.callproc('new_record_course', args=[name, user_id])
    conn.commit();
    c.callproc('find_usercourses')
    result = c.stored_results()
    course = get_list_courses(result)
    response = []
    response.append(course)
    return HttpResponse(response)

